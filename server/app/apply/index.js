const model = require('./model');
const controller = require('./controller');
const routes = require('./routes');

module.exports = (app) => {
  const redis = app.get('redis');
  const mongodb = app.get('mongodb');
  const collection = mongodb.collection('forms');
  return routes(controller(model(redis, collection)));
};
