function ApplyModel(redis, collection) {
  const getForm = id => new Promise((resolve, reject) => {
    redis.getAsync(`form:${id}`)
      .then((data) => {
        const parsed = JSON.parse(data);
        return resolve(parsed ? { form: parsed } : null);
      }).catch(err => reject(err));
  });

  const setForm = (id, data) =>
    redis.setexAsync(`form:${id}`, 48 * 60 * 60, JSON.stringify(data));

  const submitForm = (id, data) =>
    collection.insertOne(data)
      .then(redis.delAsync(`form:${id}`));

  return {
    getForm,
    setForm,
    submitForm,
  };
}

module.exports = ApplyModel;
