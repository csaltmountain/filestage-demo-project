const express = require('express');

const router = express.Router(); // eslint-disable-line

module.exports = (controller) => {
  router.get('/:id', controller.get);
  router.put('/:id', controller.set);
  router.post('/:id', controller.submit);

  return router;
};
