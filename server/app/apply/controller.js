function ApplyController(model) {
  const get = (req, res) => {
    const id = req.params.id;
    return model.getForm(id)
      .then(res.success, res.fail);
  };

  const set = (req, res) => {
    const id = req.params.id;
    const data = req.body || null;
    return model.setForm(id, data)
      .then(res.success, res.fail);
  };

  const submit = (req, res) => {
    const id = req.params.id;
    const data = req.body;
    return model.submitForm(id, data)
      .then(res.success, res.fail);
  };

  return {
    get,
    set,
    submit,
  };
}

module.exports = ApplyController;
