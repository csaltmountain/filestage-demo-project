const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const chaiAsPromised = require('chai-as-promised');

const expect = chai.expect;
chai.use(sinonChai);
chai.use(chaiAsPromised);

// Mock Error
let mockError = false;
// Mock Promise
const mockPromise = data =>
  () => new Promise((resolve, reject) => {
    if (mockError) return reject('rejected');
    return resolve(data);
  });
// Mock Redis
const mockRedis = {
  getAsync: mockPromise(JSON.stringify({ key: 'value' })),
  setexAsync: mockPromise(),
  delAsync: mockPromise(),
};
// Mock MongoDB Collection
const mockCollection = {
  insertOne: mockPromise(),
};

const model = require('./model')(mockRedis, mockCollection);

describe('Apply Model', () => {
  beforeEach(() => {
    mockError = false;
  });

  describe('#getForm()', () => {
    beforeEach(() => {
      sinon.spy(mockRedis, 'getAsync');
    });
    afterEach(() => {
      mockRedis.getAsync.restore();
    });
    it('returns a promise', () => {
      expect(model.getForm('id')).to.be.a('promise');
    });
    it('calls redis.getAsync()', () => {
      model.getForm('id');
      return expect(mockRedis.getAsync).to.have.been.calledOnce;
    });
    it('calls redis.getAsync() with `form:id`', () => {
      model.getForm('id');
      const arg = mockRedis.getAsync.getCall(0).args[0];
      return expect(arg).to.equal('form:id');
    });
    it('resolves with JSON {key: \'value\'}', () =>
      expect(model.getForm('id')).to.eventually.deep.equal({ form: { key: 'value' } })
    );
  });

  describe('#setForm()', () => {
    beforeEach(() => {
      sinon.spy(mockRedis, 'setexAsync');
    });
    afterEach(() => {
      mockRedis.setexAsync.restore();
    });
    it('returns a promise', () => {
      expect(model.setForm('id', 'data')).to.be.a('promise');
    });
    it('calls redis.setexAsync()', () => {
      model.setForm('id', 'data');
      return expect(mockRedis.setexAsync).to.have.been.calledOnce;
    });
    it('calls redis.setexAsync() with `form:id`', () => {
      model.setForm('id', 'data');
      const arg = mockRedis.setexAsync.getCall(0).args[0];
      return expect(arg).to.equal('form:id');
    });
    it('calls redis.setexAsync() with 48 hours', () => {
      model.setForm('id', 'data');
      const arg = mockRedis.setexAsync.getCall(0).args[1];
      return expect(arg).to.equal(48 * 60 * 60);
    });
    it('calls redis.setexAsync() with JSON string', () => {
      model.setForm('id', 'data');
      const arg = mockRedis.setexAsync.getCall(0).args[2];
      return expect(arg).to.equal('"data"');
    });
  });

  describe('#submitForm()', () => {
    beforeEach(() => {
      sinon.spy(mockCollection, 'insertOne');
      sinon.spy(mockRedis, 'delAsync');
    });
    afterEach(() => {
      mockCollection.insertOne.restore();
      mockRedis.delAsync.restore();
    });
    it('returns a promise', () => {
      expect(model.submitForm('id', 'data')).to.be.a('promise');
    });
    it('calls collection.insertOne()', () => {
      model.submitForm('id', 'data');
      return expect(mockCollection.insertOne).to.have.been.calledOnce;
    });
    it('calls collection.insertOne() with data', () => {
      model.submitForm('id', 'data');
      const arg = mockCollection.insertOne.getCall(0).args[0];
      return expect(arg).to.equal('data');
    });
    it('calls redis.delAsync()', () => {
      model.submitForm('id', 'data');
      return expect(mockRedis.delAsync).to.have.been.calledOnce;
    });
    it('calls redis.delAsync() with `form:id`', () => {
      model.submitForm('id', 'data');
      const arg = mockRedis.delAsync.getCall(0).args[0];
      return expect(arg).to.equal('form:id');
    });
  });
});
