const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const chaiAsPromised = require('chai-as-promised');

const expect = chai.expect;
chai.use(sinonChai);
chai.use(chaiAsPromised);

// Mock Error
let mockError = false;
// Mock Promise
const mockPromise = () =>
  params => new Promise((resolve, reject) => {
    if (mockError) return reject('rejected');
    return resolve(params);
  });
// Mock Request Object
let mockReq;
// Mock Response Object
let mockRes;
// Mock Model
const mockModel = {
  getForm: mockPromise(),
  setForm: mockPromise(),
  submitForm: mockPromise(),
};

const controller = require('./controller')(mockModel);

describe('Apply Controller', () => {
  beforeEach(() => {
    mockError = false;
    mockRes = {
      success: sinon.spy(),
      fail: sinon.spy(),
    };
    mockReq = {
      params: {
        id: 'mockId',
      },
      body: 'mockBody',
    };
  });

  describe('#get()', () => {
    beforeEach(() => {
      sinon.spy(mockModel, 'getForm');
    });
    afterEach(() => {
      mockModel.getForm.restore();
    });
    it('returns a promise', () => {
      expect(controller.get(mockReq, mockRes)).to.be.a('promise');
    });
    it('calls #model.getForm()', () => {
      controller.get(mockReq, mockRes);
      return expect(mockModel.getForm).to.have.been.calledOnce;
    });
    it('calls #model.getForm() with id', () => {
      controller.get(mockReq, mockRes);
      const id = mockModel.getForm.getCall(0).args[0];
      expect(id).to.equal('mockId');
    });
    it('calls #res.success() on resolve', () =>
      controller.get(mockReq, mockRes).then(() =>
        expect(mockRes.success).to.have.been.calledOnce
      ).catch()
    );
    it('calls #res.fail() on reject', () => {
      mockError = true;
      return controller.get(mockReq, mockRes).catch(() =>
        expect(mockRes.fail).to.have.been.calledOnce
      );
    });
  });

  describe('#set()', () => {
    beforeEach(() => {
      sinon.spy(mockModel, 'setForm');
    });
    afterEach(() => {
      mockModel.setForm.restore();
    });
    it('returns a promise', () => {
      expect(controller.set(mockReq, mockRes)).to.be.a('promise');
    });
    it('calls #model.setForm()', () => {
      controller.set(mockReq, mockRes);
      return expect(mockModel.setForm).to.have.been.calledOnce;
    });
    it('calls #model.setForm() with id', () => {
      controller.set(mockReq, mockRes);
      const id = mockModel.setForm.getCall(0).args[0];
      expect(id).to.equal('mockId');
    });
    it('calls #model.setForm() with data', () => {
      controller.set(mockReq, mockRes);
      const id = mockModel.setForm.getCall(0).args[1];
      expect(id).to.equal('mockBody');
    });
    it('calls #model.setForm() with null if no data', () => {
      delete mockReq.body;
      controller.set(mockReq, mockRes);
      const id = mockModel.setForm.getCall(0).args[1];
      expect(id).to.equal(null);
    });
    it('calls #res.success() on resolve', () =>
      controller.set(mockReq, mockRes).then(() =>
        expect(mockRes.success).to.have.been.calledOnce
      ).catch()
    );
    it('calls #res.fail() on reject', () => {
      mockError = true;
      return controller.set(mockReq, mockRes).catch(() =>
        expect(mockRes.fail).to.have.been.calledOnce
      );
    });
  });

  describe('#submit()', () => {
    beforeEach(() => {
      sinon.spy(mockModel, 'submitForm');
    });
    afterEach(() => {
      mockModel.submitForm.restore();
    });
    it('returns a promise', () => {
      expect(controller.submit(mockReq, mockRes)).to.be.a('promise');
    });
    it('calls #model.submitForm()', () => {
      controller.submit(mockReq, mockRes);
      return expect(mockModel.submitForm).to.have.been.calledOnce;
    });
    it('calls #model.submitForm() with id', () => {
      controller.submit(mockReq, mockRes);
      const id = mockModel.submitForm.getCall(0).args[0];
      expect(id).to.equal('mockId');
    });
    it('calls #model.submitForm() with data', () => {
      controller.submit(mockReq, mockRes);
      const id = mockModel.submitForm.getCall(0).args[1];
      expect(id).to.equal('mockBody');
    });
    it('calls #res.success() on resolve', () =>
      controller.submit(mockReq, mockRes).then(() =>
        expect(mockRes.success).to.have.been.calledOnce
      ).catch()
    );
    it('calls #res.fail() on reject', () => {
      mockError = true;
      return controller.submit(mockReq, mockRes).catch(() =>
        expect(mockRes.fail).to.have.been.calledOnce
      );
    });
  });
});
