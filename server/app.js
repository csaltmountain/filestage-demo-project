const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const jsend = require('./lib/jsend');

const apply = require('./app/apply');

module.exports = (connections) => {
  const app = express();

  Object.keys(connections).forEach(connection => app.set(connection, connections[connection]));

  // Enable CORS
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

    if (req.method === 'OPTIONS') return res.sendStatus(200);
    return next();
  });

  app.use(jsend);
  app.use(morgan('dev'));
  app.use(bodyParser.json());

  app.set('json spaces', 2);

  app.get('/', (req, res) => {
    res.success({ message: 'API Running' });
  });

  app.use('/apply', apply(app));

  // Handle 404
  app.use((req, res) => {
    res.error(404, 'Not Found');
  });

  return app;
};
