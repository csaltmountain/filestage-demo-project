function jsend(req, res, next) {
  const success = (data) => {
    res.json({
      status: 'success',
      data,
    });
  };
  const fail = (data) => {
    res.status(400);
    res.json({
      status: 'fail',
      data: data.message,
    });
  };
  const error = (status, message) => {
    res.status(status);
    res.json({
      status: 'error',
      message,
    });
  };

  res.success = success; //eslint-disable-line
  res.fail = fail; //eslint-disable-line
  res.error = error; //eslint-disable-line
  next();
}


module.exports = jsend;
