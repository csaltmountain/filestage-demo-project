const gulp = require('gulp');
const sass = require('gulp-sass');
const pug = require('gulp-pug');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const browserSync = require('browser-sync');
const modRewrite = require('connect-modrewrite');
const del = require('del');
const nodemon = require('gulp-nodemon');

gulp.task('build-css', () => {
  return gulp.src('./src/scss/**/*.scss').pipe(sass({
    outputStyle: 'compressed'
  })).pipe(rename({
    suffix: '.min'
  })).pipe(gulp.dest('./public/css')).pipe(browserSync.stream());
});

gulp.task('build-js', () => {
  return gulp.src('./src/js/**/*.js')
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./public/js'));
});

gulp.task('js-watch', ['build-js'], browserSync.reload);
gulp.task('build-pug', () => {
  return gulp.src('./src/pug/**/!(_)*.pug').pipe(pug({
    pretty: true
  })).pipe(gulp.dest('./public'));
});

gulp.task('pug-watch', ['build-pug'], browserSync.reload);
gulp.task('serve', ['build-pug', 'build-css', 'build-js'], () => {
  // nodemon({
  //   script: './server/bin/www',
  //   ext: 'js'
  // });
  browserSync.init({
    server: {
      baseDir: "./public",
      middleware: modRewrite(['^[^\\.]*$ /index.html [L]'])
    }
  });
  gulp.watch('./src/scss/**/*.scss', ['build-css']);
  gulp.watch('./src/js/**/*.js', ['js-watch']);
  gulp.watch('./src/pug/**/*.pug', ['pug-watch']);
});

gulp.task('clean-public', () => {
  return del(['./public/**/*', '!./public/img',
    '!./public/img/**',
    '!./public/files',
    '!./public/files/**',
    '!./public/*.php',
  ]);
});

gulp.task('default', ['clean-public'], () => {
  return gulp.start('serve');
});
