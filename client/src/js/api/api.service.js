(function() {

  angular
  .module('app')
  .service('Api', Api);

  Api.$inject = ['$http'];

  function Api($http) {

    return {
      retrieve: retrieve,
      save: save,
      submit: submit,
    };

    function retrieve(id) {
      return $http.get('http://localhost:8080/apply/' + id)
        .then(function (response) {
          return response.data.data;
        });
    }

    function save(id, data) {
      return $http.put('http://localhost:8080/apply/' + id, data);
    }

    function submit(id, data) {
      return $http.post('http://localhost:8080/apply/' + id, data);
    }
  }

})();
