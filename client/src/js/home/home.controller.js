(function () {

  angular
    .module('app')
    .controller('HomeController', HomeController);

    HomeController.$inject = ['$state', 'Api', 'Form'];

    function HomeController($state, Api, Form) {
      var vm = this;
      vm.mobile = '';
      vm.mobileRegex = /^(\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/;
      vm.submit = submit;

      function submit() {
        var id = vm.mobile.split('')
          .filter(function (value) {
            return (value === ' ' || value === '(' || value === ')') ? false : true;
          })
          .join('');

        Api.retrieve(id).then(function (data) {
          if (data) {
            Form.init(id, data.form);
          } else {
            Form.init(id);
          };
          $state.go('form.applicant');
        });
      }
    }

})();
