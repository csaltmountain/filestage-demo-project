(function () {

  angular
    .module('app')
    .controller('FormController', FormController);

    FormController.$inject = ['Form', '$state', 'Api'];

    function FormController(Form, $state, Api) {
      var vm = this;
      vm.nextForm = nextForm;
      vm.submitted = false;
      vm.submit = submit;
      vm.save = save;
      vm.data = Form.data;
      if (!vm.data || !vm.data.CourseID || !Form.id) $state.go('home');
      vm.dateRegex = /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/;
      vm.error = false;

      var nextForm = {
        applicant: 'parent',
        parent: 'school',
        school: vm.data.AdditionalFields ? 'additional' : 'invoice',
        invoice: 'review',
        additional: 'review',
      };

      function nextForm(state) {
        Api.save(Form.id, vm.data)
          .then(function () {
            $state.go('^.' + nextForm[state], {id: vm.data.CourseID});
          });
      }

      function save(state) {
        Api.save(Form.id, vm.data)
          .then(function () {
            Form.id = null;
            $state.go('home');
          });
      }

      function submit() {
        vm.submitted = true;
        Api.submit(Form.id, Form.data).then(function (data) {
          $state.go('success');
          Form.init();
        }).catch(function () {
          vm.error = true;
        });
      }
    }

})();
