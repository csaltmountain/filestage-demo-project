(function() {

  angular
  .module('app')
  .service('Form', Form);

  function Form() {

    // Array of course ID that have additional field templates
    var courses = [
      11787
    ];

    var id = null;
    var data = null;

    return {
      id: id,
      data: data,
      init: init,
      storeId: storeId,
      getId: getId,
    };

    function init(formId, formData) {
      this.id = formId || null;
      this.data = formData || {
        CourseID: 1234,
        CourseAppliedFor: 'Example Course',
        FirstName: '',
        Surname: '',
        DOB: '',
        Gender: '',
        SchoolYear: '',
        Address: '',
        Address1: '',
        Address2: '',
        Postcode: '',
        Country: 'GB',
        Nationality: 'british',
        Ethnicity: '11',
        Email: '',
        Mobile: formId,
        Tel: '',

        ParentTitle: '',
        ParentFirstName: '',
        ParentSurname: '',
        ParentSameAddress: true,
        ParentAddress: '',
        ParentAddress1: '',
        ParentAddress2: '',
        ParentPostcode: '',
        ParentCountry: 'GB',
        ParentEmail: '',
        ParentMobile: '',
        ParentTelephone:'',

        SchoolType: '',
        SchoolName: '',
        SchoolAddress: '',
        SchoolAddress1: '',
        SchoolAddress2: '',
        SchoolPostcode: '',
        SchoolCountry: 'GB',
        TeacherTitle: '',
        TeacherFirstName: '',
        TeacherSurname: '',
        TeacherPosition: '',
        TeacherEmail: '',

        InvoiceType: '',
        InvoiceFirstName: '',
        InvoiceSurname: '',
        InvoiceOrganisation: '',
        InvoiceAddress: '',
        InvoiceAddress1: '',
        InvoiceAddress2: '',
        InvoicePostcode: '',
        InvoiceCountry: 'GB',

        OptOutEmailContact: false,
        ParentAccept:false,
        TCAccept: false,

        AdditionalInformation: [],

        AdditionalFields: false
      };
    }

    function storeId() {
      sessionStorage.setItem('id', id);
    }

    function getId() {
      sessionStorage.getItem('id');
    }
  }

})();
