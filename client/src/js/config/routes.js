(function(){

  angular
  .module('app')
  .config(routes);

  routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

  function routes($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
    .state('home', {
      url: '/?{id:int}&title',
      templateUrl: 'partials/home.html',
      controller: 'HomeController as home'
    })
    .state('form', {
      abstract: true,
      templateUrl: 'partials/forms/form.html',
      controller: 'FormController as form'
    })
    .state('form.applicant', {
      url: '/applicant',
      templateUrl: 'partials/forms/applicant.html',
    })
    .state('form.parent', {
      url: '/parent',
      templateUrl: 'partials/forms/parent.html',
    })
    .state('form.school', {
      url: '/school',
      templateUrl: 'partials/forms/school.html',
    })
    .state('form.invoice', {
      url: '/invoice',
      templateUrl: 'partials/forms/invoice.html',
    })
    .state('form.additional', {
      url: '/:id/additional',
      templateUrl: function (params) {
        return 'partials/forms/additional/' + params.id + '.html'
      },
    })
    .state('form.review', {
      url: '/review',
      templateUrl: 'partials/forms/review.html',
    })
    .state('success', {
      url: '/success',
      templateUrl: 'partials/success.html',
    })
    .state('error', {
      url: '/error',
      templateUrl: 'partials/error.html'
    });

    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(false);
  }
})();
