(function() {

  angular
    .module('app', [
      'ui.router'
    ])
    .run(run);

    run.$inject = ['$rootScope'];

    function run($rootScope) {
      $rootScope.$on('$stateChangeSuccess', function () {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      });
    }

})();
